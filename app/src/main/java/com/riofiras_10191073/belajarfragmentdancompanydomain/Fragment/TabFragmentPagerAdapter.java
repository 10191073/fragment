package com.riofiras_10191073.belajarfragmentdancompanydomain.Fragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {
    //nama tab nya
    String[] title = new String[]{
            "Tab 1", "Tab 2"
    };

    public TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    //Method ini yang akan memanipulasi penampilan com.riofiras_10191073.belajarfragmentdancompanydomain.Fragment dilayar 10191073_RioFiras
    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new BlankFragment1();
                break;
            case 1:
                fragment = new BlankFragment2();
                break;
            default:
                fragment = null;
                break;
        }
        return fragment;
 }

 @Override
 public CharSequence getPageTitle(int position) {
    return title[position];
 }

 @Override
 public int getCount() {
     return title.length;
 }

}
